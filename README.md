# jQuery Mobile Miletracker App #

Simple jQuery Mobile app that allows you to add the number of miles you've run on a given date. Uses the HTML5 localStorage object. From Eduonix's [**Projects In JavaScript & JQuery course**](https://stackskills.com/courses/projects-in-javascript-jquery) on **Stackskills**.

## Initial Load ##
![screencapture-localhost-jquery_mobile_miletracker_app-1453956892830.png](https://bitbucket.org/repo/zdxEpB/images/3893731535-screencapture-localhost-jquery_mobile_miletracker_app-1453956892830.png)

## Add Run ##
![screencapture-localhost-jquery_mobile_miletracker_app-1453956904465.png](https://bitbucket.org/repo/zdxEpB/images/2103373732-screencapture-localhost-jquery_mobile_miletracker_app-1453956904465.png)

## Load Runs from Local Storage##
![Screen Shot 2016-01-27 at 11.56.14 PM.png](https://bitbucket.org/repo/zdxEpB/images/1521446681-Screen%20Shot%202016-01-27%20at%2011.56.14%20PM.png)