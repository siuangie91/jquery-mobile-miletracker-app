$(document).one('pageinit', function() {
    // display runs
    showRuns();
    
    // Add handler
    $('#submitAdd').on('tap', addRun);
    
    // Edit handler
    $('#submitEdit').on('tap', editRun);
    
    // Delete handler
    $('#stats').on('tap', '#deleteLink', deleteRun);
    
    // Set Current handler
    $('#stats').on('tap', '#editLink', setCurrent);
    
    // Clear handler
    $('#clearRuns').on('tap', clearRuns);
    
    // show all runs on homepage function
    function showRuns() {
        //get runs object from localStorage
        var runs = getRunsObject();
        //check if empty
        if(runs != "" && runs != null) {
            for(var i = 0; i < runs.length; i++) {
                $('#stats').append('<li class="ui-body-inherit ui-li-static"><strong>Date: </strong>'+runs[i]['date']+'<br><strong>Distance: </strong>'+runs[i]['miles']+'mi<div class="controls"><a href="#edit" id="editLink" data-miles="'+runs[i]['miles']+'" data-date="'+runs[i]['date']+'">Edit</a> | <a href="#" id="deleteLink" data-miles="'+runs[i]['miles']+'" data-date="'+runs[i]['date']+'" onclick="return confirm(\'Are you sure you want to delete?\')">Delete</a></div></li>');
            }
            $('#home').bind('pageinit', function() {
                $('#stats').listview('refresh');    
            });
        } else {
            $('#stats').html('<p>You have no logged runs</p>');
        }
    }
    
    // Add run function
    function addRun() {
        // get form values
        var miles = $('#addMiles').val();
        var date = $('#addDate').val();
        
        // create 'run' object
        var run = {
            date: date,
            miles: parseFloat(miles)
        }
        
        // get the current run's obj from localStorage
        var runs = getRunsObject();
        
        // add run to runs array
        runs.push(run);
        alert('Run added');
        
        // stringify object for localStorage, which only takes strings
        localStorage.setItem('runs', JSON.stringify(runs));
        
        // redirect to index page
        window.location.href="index.html";
        
        return false;
    }
    
    //editRun function
    function editRun() {
        // get current data
        currentMiles = localStorage.getItem('currentMiles');
        currentDate = localStorage.getItem('currentDate');
        
        // get the current run's obj from localStorage
        var runs = getRunsObject();
        
        //loop through all runs
        for(var i = 0; i < runs.length; i++) {
            //if loop iteration equals current value
            if(runs[i].miles == currentMiles && runs[i].date == currentDate) {
                //take it out
                runs.splice(i, 1);    
            }
            //then put them back in runs
            localStorage.setItem('runs', JSON.stringify(runs));
        }
    
        
        // get new values from form
        var miles = $('#editMiles').val();
        var date = $('#editDate').val();
        
        // put into 'run' object
        var udpateRun = {
            date: date,
            miles: parseFloat(miles)
        }
        
        // add run to runs array
        runs.push(udpateRun);
        alert('Run udpated');
        
        // stringify object for localStorage, which only takes strings
        localStorage.setItem('runs', JSON.stringify(runs));
        
        // redirect to index page
        window.location.href="index.html";
        
        return false;
    }
    
    //deleteRun function
    function deleteRun() {
        //set localStorage items
        localStorage.setItem('currentMiles', $(this).data('miles'));
        localStorage.setItem('currentDate', $(this).data('date'));
        
        // get current data
        currentMiles = localStorage.getItem('currentMiles');
        currentDate = localStorage.getItem('currentDate');
        
        // get the current run's obj from localStorage
        var runs = getRunsObject();
        
        //loop through all runs
        for(var i = 0; i < runs.length; i++) {
            //if loop iteration equals current value
            if(runs[i].miles == currentMiles && runs[i].date == currentDate) {
                //take it out
                runs.splice(i, 1);    
            }
            //then put them back in runs
            localStorage.setItem('runs', JSON.stringify(runs));
        }
    
        alert('Run deleted');
        
        // stringify object for localStorage, which only takes strings
        localStorage.setItem('runs', JSON.stringify(runs));
        
        // redirect to index page
        window.location.href="index.html";
        
        return false;
    }
    
    //clearRuns function
    function clearRuns() {
        localStorage.removeItem('runs');
        $('#stats').html('<p>You have no logged runs</p>');
    }
    
    // getRunsObject function
    function getRunsObject() {
        //set runs array
        var runs = [];
        //get current runs from localStorage
        var currentRuns = localStorage.getItem('runs');
        //check to see if there is anything in currentRuns
        if(currentRuns != null) {
            //parse currentRuns into JSON
            var runs = JSON.parse(currentRuns);    
        }
        //return runs object with newest runs on top
        return runs.sort(function(a, b) {
            return new Date(b.date) - new Date(a.date);    
        });
    }
    
    // setCurrent function - set current clicked miles and date
    function setCurrent() {
        //set localStorage items
        localStorage.setItem('currentMiles', $(this).data('miles'));
        localStorage.setItem('currentDate', $(this).data('date'));
        
        //insert these items into the edit form fields
        $('#editMiles').val(localStorage.getItem('currentMiles'));
        $('#editDate').val(localStorage.getItem('currentDate'));
    }
});